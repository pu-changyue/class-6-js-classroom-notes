# Map集合
    Map 对象保存键值对，并且能够记住键的原始插入顺序。任何值（对象或者原始值）都可以作为键或值。

    Map 对象是键值对的集合。Map 中的一个键只能出现一次；它在 Map 的集合中是独一无二的。Map 对象按键值对迭代——一个 for...of 循环在每次迭代后会返回一个形式为 [key, value] 的数组。迭代按插入顺序进行，即键值对按 set() 方法首次插入到集合中的顺序（也就是说，当调用 set() 时，map 中没有具有相同值的键）进行迭代。

## 构造函数
    Map()
    创建 Map 对象。

## 静态属性
    Map[@@species]
    用于创建派生对象的构造函数。

## 静态方法
    Map.groupBy()
    根据提供的回调函数返回的值将给定的可迭代对象分组。最终返回的 Map 对象使用测试函数返回的唯一值作为键，可用于获取每个组的元素数组。

## 实例属性
    这些属性在 Map.prototype 上定义，并由所有 Map 实例共享。

    Map.prototype.constructor
    创建实例对象的构造函数。对于 Map 实例，初始值为 Map 构造函数。

    Map.prototype.size
    返回 Map 对象中的键值对数量。

    Map.prototype[@@toStringTag]
    @@toStringTag 属性的初始值是字符串 "Map"。该属性在 Object.prototype.toString() 中使用。

## 实例方法
    Map.prototype.clear()
    移除 Map 对象中所有的键值对。

    Map.prototype.delete()
    移除 Map 对象中指定的键值对，如果键值对存在并成功被移除，返回 true，否则返回 false。调用 delete 后再调用 map.has(key) 将返回 false。

    Map.prototype.entries()
    返回一个新的迭代器对象，其包含 Map 对象中所有键值对 [key, value] 二元数组，以插入顺序排列。

    Map.prototype.forEach()
    以插入顺序为 Map 对象中的每个键值对调用一次 callbackFn。如果为 forEach 提供了 thisArg 参数，则它将作为每一次 callback 的 this 值。

    Map.prototype.get()
    返回与指定的键 key 关联的值，若不存在关联的值，则返回 undefined。

    Map.prototype.has()
    返回一个布尔值，用来表明 Map 对象中是否存在与指定的键 key 关联的值。

    Map.prototype.keys()
    返回一个新的迭代器对象，其包含 Map 对象中所有元素的键，以插入顺序排列。

    Map.prototype.set()
    在 Map 对象中设置与指定的键 key 关联的值，并返回 Map 对象。

    Map.prototype.values()
    返回一个新的迭代对象，其中包含 Map 对象中所有的值，并以插入 Map 对象的顺序排列。

    Map.prototype[@@iterator]()
    返回一个新的迭代器对象，其包含 Map 对象中所有元素 [key, value] 二元数组，以插入顺序排列。

